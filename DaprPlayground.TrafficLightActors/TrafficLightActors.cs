﻿using System.Text.Json;
using Dapr.Actors;
using Dapr.Actors.Client;
using Dapr.Actors.Runtime;
using Dapr.Client;

namespace DaprPlayground.TrafficLightActors;

public class TrafficLightControllerActor: Actor, ITrafficLightControllerActor
{
    readonly ILogger<TrafficLightControllerActor> _logger;
    readonly DaprClient _daprClient;

    public TrafficLightControllerActor(ILogger<TrafficLightControllerActor> logger, DaprClient daprClient, ActorHost host) : base(host)
    {
        _logger = logger;
        _daprClient = daprClient;
    }

    public async Task Configure(IEnumerable<string> trafficLights)
    {
        var config = new Configuration(trafficLights.ToArray());
        await StateManager.AddOrUpdateStateAsync("configuration", config, (_, _) => config);

        foreach (var light in config.TrafficLights)
        {
            await _daprClient.PublishEventAsync("pubsub", "trafficLightConfigured",
                new TrafficLightConfiguredEvent(light, Id.GetId()));
        }
    }

    public async Task Start()
    {
        var lightState = await GetLightStateAsync();
        await SetChildLightsAsync(lightState);
        await SetTickTimerAsync(lightState);
    }
    
    async Task SetTickTimerAsync(LightState lightState)
    {
        var duration = GetTickDuration(lightState.IsCrossSignalActive);
        await RegisterTimerAsync("tick", nameof(Tick), Array.Empty<byte>(), duration, TimeSpan.FromMilliseconds(-1));
        
        TimeSpan GetTickDuration(bool isBeeperActive) => isBeeperActive switch
        {
            false => TimeSpan.FromSeconds(5),
            true => TimeSpan.FromSeconds(10)
        };
    }

    public async Task ActivateCallButton()
    {
        await StateManager.AddOrUpdateStateAsync("lightState", LightState.Initial, (_,p) =>
        {
            p.IsCallSignalActive = true;
            return p;
        });
        var state = await StateManager.GetStateAsync<LightState>("lightState");
        await StateManager.SaveStateAsync();
        await SetChildLightsAsync(state);
    }

    async Task SetChildLightsAsync(LightState lightState)
    {
        var configuration = await GetConfigurationAsync();
        var proxies = configuration.TrafficLights.Select(CreateTrafficLightProxy);
        foreach (var trafficLightActor in proxies)
        {
            await trafficLightActor.SetLights(lightState);
        }
    }

    ITrafficLightActor CreateTrafficLightProxy(string p)
    {
        return ProxyFactory.CreateActorProxy<ITrafficLightActor>(new ActorId(p), nameof(TrafficLightActor));
    }

    async Task<Configuration> GetConfigurationAsync()
    {
        return await StateManager.TryGetStateAsync<Configuration>("configuration") switch
        {
            { HasValue: false } => new Configuration(Array.Empty<string>()),
            var state => state.Value
        };
    }

    async Task<LightState> GetLightStateAsync() => await StateManager.GetOrAddStateAsync("lightState", LightState.Initial);

    async Task SetLightStateAsync(LightState lightState) => await StateManager.SetStateAsync("lightState", lightState);

    public async Task Tick()
    {
        _logger.LogInformation("Traffic light {Id} ticked from a timer", Id);
        var currentState = await GetLightStateAsync();
        var nextState = currentState switch
        {
            { Red: true, Yellow: false, Green: false } => new LightState { Red = true, Yellow = true },
            { Red: true, Yellow: true, Green: false } => new LightState { Green = true },
            { Red: false, Yellow: false, Green: true } => new LightState { Yellow = true },
            { Red: false, Yellow: true, Green: false } => new LightState { Red = true, IsCrossSignalActive = currentState.IsCallSignalActive, IsCallSignalActive = false},
            _ => throw new Exception($"Current light state is invalid: {currentState}")

        };
        
        _logger.LogInformation("Controller {Id} changing light state to {LightState}", Id, JsonSerializer.Serialize(nextState));
        await SetLightStateAsync(nextState);
        await SetChildLightsAsync(nextState);
        await SetTickTimerAsync(nextState);
    }

    record Configuration(string[] TrafficLights);
}

public class LightState
{
    public bool Red { get; init; }
    public bool Yellow { get; set; }
    public bool Green { get; set; }
    public static LightState Initial { get; } = new() { Red = true };
    public bool IsCallSignalActive { get; set; }
    public bool IsCrossSignalActive { get; set; }
}

public class TrafficLightActor : Actor, ITrafficLightActor
{
    readonly ILogger<TrafficLightActor> _logger;
    readonly DaprClient _daprClient;

    public TrafficLightActor(ILogger<TrafficLightActor> logger, DaprClient daprClient, ActorHost host): base(host)
    {
        _logger = logger;
        _daprClient = daprClient;
    }

    public Task PushBeeper() => Task.CompletedTask;

    public async Task SetLights(LightState lightState)
    {
        _logger.LogInformation("Traffic Light {Id} changing light state to {LightState}", Id, JsonSerializer.Serialize(lightState));
        await StateManager.AddOrUpdateStateAsync("lightState", lightState, (_, _) => lightState);
        await _daprClient.PublishEventAsync("pubsub", "trafficLightChanged",
            new TrafficLightChangedEvent(Id.GetId(), lightState));
    }
}

public record TrafficLightChangedEvent(string LightId, LightState LightState);
public record TrafficLightConfiguredEvent(string TrafficLightId, string lightControllerId);

public interface ITrafficLightControllerActor: IActor
{
    Task Configure(IEnumerable<string> trafficLights);
    Task Start();
    Task ActivateCallButton();
}

public interface ITrafficLightActor : IActor
{
    public Task PushBeeper();
    Task SetLights(LightState lightState);
}

class TrafficLightBootstrapper: BackgroundService
{
    readonly ILogger<TrafficLightBootstrapper> _logger;
    readonly IActorProxyFactory _actorProxyFactory;

    public TrafficLightBootstrapper(ILogger<TrafficLightBootstrapper> logger, IActorProxyFactory actorProxyFactory)
    {
        _logger = logger;
        _actorProxyFactory = actorProxyFactory;
    }
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (true)
        {
            if (stoppingToken.IsCancellationRequested) break;
            try
            {
                var proxy = _actorProxyFactory.CreateActorProxy<ITrafficLightControllerActor>(new ActorId("tl1"), nameof(TrafficLightControllerActor));
                await proxy.Configure(new [] { "lightA", "lightB" });
                await proxy.Start();
                return;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An error occurred whilst invoking an actor");
            }
            await Task.Delay(1000, stoppingToken);
        }
    }
}