﻿using Dapr;
using Dapr.Actors;
using Dapr.Actors.Client;
using Dapr.Client;
using Microsoft.AspNetCore.Mvc;

namespace DaprPlayground.TrafficLightActors;

[ApiController]
public class TrafficLightsController: Controller
{
    string _storeName = "statestore";

    [Topic("pubsub", "trafficLightChanged")]
    [HttpPost("/trafficLightChanged")]
    public async Task<ActionResult> TrafficLightStateChanged(TrafficLightChangedEvent @event, [FromServices]ILogger<TrafficLightsController> logger)
    {
        logger.LogInformation("Traffic light changed event received: {@State}", new
        {
            @event.LightId, LightState = new
            {
                @event.LightState.Red, 
                @event.LightState.Yellow, 
                @event.LightState.Green,
                CallState = @event.LightState.IsCallSignalActive
            }
        });
        await Task.CompletedTask;
        return new OkResult();
    }
    
    [Topic("pubsub","trafficLightConfigured")]
    [HttpPost("/trafficLightConfigured")]
    public async Task<ActionResult> TrafficLightConfigured(TrafficLightConfiguredEvent @event, [FromServices]DaprClient daprClient)
    {
        var lightConfigurationState = new LightConfiguration(new LightId(@event.TrafficLightId), @event.lightControllerId);
        var stateEntry =
            await daprClient.GetStateEntryAsync<LightConfigurations>(_storeName, LightConfigurations.Id, ConsistencyMode.Strong);
        var currentState = stateEntry.Value;
        var dictionary = new Dictionary<LightId, LightConfiguration>(currentState.LightConfigurationStates)
            {
                [new LightId(@event.TrafficLightId)] = lightConfigurationState
            };
        var newState = new LightConfigurations(dictionary);
        await daprClient.SaveStateAsync(_storeName, LightConfigurations.Id, newState);
        return new OkResult();
    }

    [HttpPost("/activateCallSignal")]
    public async void ActivateCallSignal([FromBody]CallSignalCommand callSignalCommand, [FromServices] DaprClient daprClient)
    {
        var configurationState = await daprClient.GetStateAsync<LightConfigurations>(_storeName, LightConfigurations.Id);
        var lightId = new LightId(callSignalCommand.TrafficLightId);
        var controllerId = configurationState.LightConfigurationStates[lightId].ControllerId;
        var controllerActorId = new ActorId(controllerId);
        var proxy = new ActorProxyFactory().CreateActorProxy<ITrafficLightControllerActor>(controllerActorId, nameof(TrafficLightControllerActor));
        await proxy.ActivateCallButton();
    }

    public class CallSignalCommand
    {
        public string TrafficLightId { get; set; } = "";
    }

    public record LightId(string Value);
    public record LightConfiguration(LightId LightId, string ControllerId);

    public record LightConfigurations(Dictionary<LightId, LightConfiguration> LightConfigurationStates)
    {
        public static string Id = "all-lights";
    }
}